import React from 'react';
import './App.css';

import Header from './components/Header';
import MainView from './components/MainView';

const App = () =>
  <div className="App">
    <Header />
    <MainView />
  </div>

export default App;
