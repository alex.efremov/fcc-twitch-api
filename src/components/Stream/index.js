import React from 'react';

import './Stream.css';

const Stream = ({ viewers }) =>
<div>
  <span className='right floated'>
    <div className={`led ${viewers ? 'led-on' : 'led-off'}`}></div>
  </span>
  <span className='right floated' style={{ marginTop: '20px'}}>
    <div>{viewers && `${viewers} viewers`}</div>
  </span>
</div>

export default Stream;
