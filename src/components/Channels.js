import React from 'react';
import { withPropsOnChange } from 'recompose';

import Channel from './Channel';

const enhance = withPropsOnChange(
  ['filter'],
  ({ filter, channels }) => {
    switch (filter) { //eslint-disable-line
      case 'Online':
        return {
          channels: channels.filter(channel => !!channel.stream)
        }
      case 'Offline':
        return {
          channels: channels.filter(channel => !channel.stream)
        }
    }
  }
)

const Channels = ({ channels }) =>
  <div className='ui link cards centered'>
    {channels.map(channel => <Channel key={channel.name} {...channel} /> )}
  </div>

export default enhance(Channels);
