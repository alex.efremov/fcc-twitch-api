import React from 'react';
import { branch, renderComponent } from 'recompose';

import Stream from './Stream';

const placeholder = 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMiAzMiIgd2lkdGg9IjMyIiBoZWlnaHQ9IjMyIiBmaWxsPSJ3aGl0ZSI+CiAgPHBhdGggZD0iTTAgNCBMMCAyOCBMMzIgMjggTDMyIDQgeiBNNCAyNCBMMTAgMTAgTDE1IDE4IEwxOCAxNCBMMjQgMjR6IE0yNSA3IEE0IDQgMCAwIDEgMjUgMTUgQTQgNCAwIDAgMSAyNSA3Ij48L3BhdGg+Cjwvc3ZnPg=='



const NotExistedChannel = ({ name }) =>
<div className='card'>
  <div className='image'>
    <img
      src={placeholder}
      alt='logo' />
  </div>
  <div className='content'>
    <div className='header'>{name}</div>
    <div className='description'>
      Channel doesn't exist
    </div>
  </div>
</div>


const Channel = ({ logo, status, url, name, game, bio, followers, stream }) =>
  <a href={url} target='_blank' rel='noopener noreferrer' className='card'>
    <div className='image'>
      <img
        src={logo || placeholder}
        alt='logo' />
    </div>
    <div className='content'>
      <div className='header'>{name}</div>
      <div className='meta'>{game}</div>
      <div className='description'>
        {bio || 'No description'}
      </div>
    </div>
    <div className='extra content'>
      <span>
        <i className='user icon'></i>
        {followers} Followers
      </span>
      <Stream {...stream}/>
    </div>
  </a>

const enhance = branch(
  ({ status }) => status === 404,
  renderComponent(NotExistedChannel)
);

export default enhance(Channel);
