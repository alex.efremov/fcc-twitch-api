import React from 'react';
import {
  compose,
  withState,
  defaultProps,
  withProps
} from 'recompose';

const enhance = compose(
  defaultProps({
    defaultTabIndex: 0
  }),
  withState('activeTabIndex', 'setTabIndex', ({ defaultTabIndex }) => defaultTabIndex),
  withProps(({ children, setTabIndex, activeTabIndex }) => ({
    children: React.Children.map(children, (child, index) => {
      return React.cloneElement(child, {
        onClick: setTabIndex,
        index,
        isActive: index === activeTabIndex
      });
    })
  }))
);

const Tabs = ({ children, activeTabIndex }) =>
  <div>
    <div className="ui top attached tabular menu">
      {children}
    </div>
    <div className="ui bottom attached active tab segment">
      {children[activeTabIndex].props.children}
    </div>
  </div>

export default enhance(Tabs);
