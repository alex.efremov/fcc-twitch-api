import React from 'react';
import { compose, onlyUpdateForKeys, withProps, withHandlers } from 'recompose';

const enhance = compose(
  onlyUpdateForKeys(['isActive']),
  withProps(({ isActive }) => ({
    clName: isActive ? 'active' : ''
  })),
  withHandlers({
    onClick: ({ index, onClick }) => () => onClick(index)
  })
);

const Tab = ({ onClick, clName, label }) => {
  return (
    <div
      className={`${clName} item`}
      onClick={onClick} >
      {label}
    </div>

  )
}

export default enhance(Tab);
