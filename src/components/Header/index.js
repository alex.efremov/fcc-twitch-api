import React from 'react';
import MediaQuery from 'react-responsive'

import './Header.css';
import logo from './logo.svg';
import logoGlitch from './logo-glitch.svg';

const Header = () =>
<div className='ui container'>
  <div className='Header'>
    <MediaQuery maxDeviceWidth={767}>
      <img src={logoGlitch} alt="logo" />
    </MediaQuery>
    <MediaQuery minDeviceWidth={768}>
      <img src={logo} alt="logo" />
    </MediaQuery>
  </div>
</div>

export default Header;
