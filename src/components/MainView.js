import React from 'react';

import Tabs from './Tabs';
import Tab from './Tab';
import { getChannel } from '../utils/api';
import channels from '../channels';
import Channels from './Channels';


class MainView extends React.Component {
  constructor() {
    super();
    this.state = {
      channels: [],
      filter: 'All'
    }
  }

  componentDidMount() {
    channels.map(name => getChannel(name))
    .reduce((seq, req) => {
      return seq.then(() => req.then(channel => {
        if (channel) {
          this.setState({
            channels: [
              ...this.state.channels,
              channel
            ]
          });
        }
      }));
    }, Promise.resolve());
  }

  render() {
    return (
      <div className='ui container'>
        <Tabs>
          <Tab label='All'>
            <Channels channels={this.state.channels} filter='All' />
          </Tab>
          <Tab label='Online'>
            <Channels channels={this.state.channels} filter='Online' />
          </Tab>
          <Tab label='Offline'>
            <Channels channels={this.state.channels} filter='Offline' />
          </Tab>
        </Tabs>
      </div>
    );
  }
}

export default MainView;
