import $ from "jquery";

const rootURL = 'https://wind-bow.gomix.me/twitch-api';

const api = {
  get: query => $.ajax({
    url: `${rootURL}${query}`,
    dataType: 'jsonp'
  })
};


export async function getChannel(name) {
  try {
    const channel = await api.get(`/channels/${name}`);
    if (channel.status === 404) {
      return {
        name,
        status: channel.status
      }
    }

    const [{ stream }, { bio }] = await Promise.all([
      api.get(`/streams/${name}`),
      api.get(`/users/${name}`)
    ]);

    return {
      name: channel.display_name,
      followers: channel.followers,
      logo: channel.logo,
      game: channel.game,
      url: channel.url,
      bio,
      stream: stream ? {
        viewers: stream.viewers
      } : null
    }
  }
  catch (e) {
    console.log(e);
  }
};
